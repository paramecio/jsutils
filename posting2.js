

(function( $ ){
	
    /* Add csrf token */
    
    /* options: url: url to post, loading: dom id, success: func, pre_callback, separated_data:boolean */
    
    $.fn.options={};
    
	$.fn.sendPost = function (options) 
	{

        if(!options.hasOwnProperty("url")) {
            
            throw 'Error: you need an url for use sendPost';
            
            return false;
            
        }
        
        if(!options.hasOwnProperty("loading")) {
            
            options['loading']='#loading';
            
        }
        
        this.options=options;
        
        $(this).on('submit', {url: this.options.url}, function (e) {

            //Get data of form
            
            form=this;
            
            /*if($(this).attr('enctype'))
            {*/
                if(options.separated_data)
                {
             
                    data[options.separated_data]=new FormData($(this)[0]); 
                    
                }
                else
                {
                    
                    data=new FormData($(this)[0]); 
                    
                }
                
                
                xhrFields={
                  withCredentials: true
                };
                
            /*}
            else
            {

                if(options.separated_data)
                {
            
                    data[options.separated_data]=$(this).serializeArray(); 
                    
                }
                else
                {
                
                    data=$(this).serializeArray().reduce(function(obj, item) {
                        obj[item.name] = item.value;
                        return obj;
                    }, {});
                    
                }
                
                xhrFields={
                };

            }*/
            
            if(!data.hasOwnProperty("csrf_token")) {
            
                data['csrf_token']=$('#csrf_token_generic').val();
                
            }
            
            //Hide form and show the time icon
            
            $(this).find('.error').hide();
            //$(this).hide();
            $(options.loading).show();
            
            /*$(this).find('input').prop("disabled", true);
            $(this).find('select').prop("disabled", true);*/
            
            if(options.hasOwnProperty("pre_callback")) {
            
                options.pre_callback(data);
                
            }
            
            //Ajax
            
            $.ajax({
                type: "POST",
                url: options.url,
                data: data,
                encoding: "UTF-8", 
                xhrFields: xhrFields,
                cache:false,
                contentType: false,
                processData: false,
                success: function (data) {
                
                    $(options.loading).hide();
                
                    
                    if(data.error) {
                        
                        if(!data.hasOwnProperty("error_csrf")) 
                        {    


                            if(data.hasOwnProperty("csrf_token")) 
                            {

                                $('.csrf_token').attr('value', data.csrf_token);
                                    
                            }
                                                    
                            
                            console.log('Error');
                            
                            if(data.hasOwnProperty('error_form'))
                            {
                                for(i in data.error_form) {

                                    $(i).html(data.error_form[i]);
                                    $(i).show();
                                    
                                }
                            }
                            else
                            if(data.hasOwnProperty('form'))
                            {
                                
                                for(i in data.form) {

                                    $('#'+i+'_error').html(data.form[i]);
                                    $('#'+i+'_error').show();
                                }
                                
                            }
                            
                            if(options.hasOwnProperty("error_data")) {
                                
                                options.error_data(data);
                            }
                            
                            $(form).show();
                        }
                        else
                        {
                            
                            alert('You session expired, the page was reload by security');
                            location.reload();

                            
                        }
                        
                    }
                    else
                    {

                        
                    
                        // If ok then post successful task.
                        
                        if(options.hasOwnProperty("success")) {
                
                            options.success(data);
                            
                        }
                    }
                },
                error: function (data) {
                    
                    //Show the form again
                    
                    if(data.hasOwnProperty("csrf_token")) 
                    {

                        $('.csrf_token').attr('value', data.csrf_token);
                            
                    }
                    
                    $(options.loading).hide();                    
                    alert('Error: '+data.status+' '+data.statusText);

                    
                },
                dataType: 'json'
            });
            
            return false;
            
        });
        
        return this;
        
    }

})( jQuery );
