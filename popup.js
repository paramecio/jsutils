$=jQuery;

(function( $ ){
    
	$.fn.popUp = function (popup, width, pre_callback, post_callback, options) {

        var popup_container='body';
        
        var after_show=false;
        
        var after_click=false;

        if(options!=undefined)
        {

            if(options.hasOwnProperty("popup_container")) 
            {

                popup_container=options.popup_container;
                
            }

            if(options.hasOwnProperty("after_show")) 
            {

                after_show=options.after_show;
                
            }

            if(options.hasOwnProperty("after_click")) 
            {

                after_click=options.after_click;
                
            }
            
            
        }
        
        //If scroll put in center scroll, if not scroll, put in simple center
        
        $(popup).hide();

        $(this).on('click', function (e) {
            

            body_height=$("body").height();

            $('#layer_popup').height(body_height);
            
            $('body').prepend('<div id="layer_popup"></div>');
            
            $('body').css('overflow', 'hidden');

            content_popup=$(popup_container).prepend('<div class="content_popup"><div class="lds-facebook"><div></div><div></div><div></div></div></div>');
            
            $('.lds-facebook').show();
            
            $(popup).appendTo('.content_popup');

            if(width==undefined)
            {

                $('.content_popup').width('80%');
                
            }
            else
            {

                $('.content_popup').width(width);                
                
            }
            
            popup_height=parseInt($(popup).height())+40;
                
            window_height=$(window).height();
            
            $('.content_popup').wrap('<div id="container_popup"></div>');

            $('#container_popup').css('height', window_height);
            
            $('#container_popup').addClass('click_popup');
            $('#layer_popup').addClass('click_popup');
            
            pos_scroll=$(document).scrollTop();
                
            new_top=Math.round((window_height-popup_height)/2)+pos_scroll;

            $('#container_popup').css('top', pos_scroll);
            
            if (popup_height>window_height) 
            {

                $('.content_popup').css('margin-top', 0);

            }
            
            $('#error_insert_name').html('')            
            
            $('.close_popup').click(function () {
               
                $('#container_popup').click();
                
                if(after_click)
                {
                    
                    after_click();
                    
                }
                
                return false;
                
            });
            
            $('.click_popup').click(function (data) {
				
                if (data.target == this) {
                
                    $(popup).hide();
                    
                    $(popup).appendTo('body');
                    
                    $('#layer_popup').remove();
                    
                    $('#container_popup').remove();
                    
                    //$("body > #height-helper").contents().unwrap();
                    
                    $('#height-helper').remove();
                    
                    $('body').css('height', '100%');

                    $('body').css('overflow', 'auto');
                    
                    if(post_callback!=undefined)
                    {
                        
                        post_callback();
                        
                    }
                    
                    $('.close_popup').unbind('click');

                    if(after_click)
                    {
                        
                        after_click();
                        
                    }
                    
                    
                }
                else {
                
                    return true;
                
                }
                
                return false;
			
			});
            
            if(pre_callback!=undefined)
            {
                
                r=pre_callback(popup, this);
                
                if(r!=undefined)
                {
                        
                    if(r===false)
                    {
                        
                        return false;
                        
                    }
                    
                }
                
            }
            
            if(!after_show)
            {
            
                $(popup).fadeIn();
                
            }
            else
            {
                
                $(popup).fadeIn({'done': after_show(popup)});                
                
            }
            
            $('.lds-facebook').hide();
            
            
            return false;
            
        });
    
    }
	
})( jQuery );
