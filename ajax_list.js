$=jQuery;

(function( $ ){
	
    /* Add csrf token */
    
    /* options: url: url to post, loading: dom id, success: func, pre_callback, separated_data:boolean */
    
	$.fn.ajaxList = function (name_list, options) 
	{
        table_base='<table class="table_list" id="'+name_list+'"> \
        </table>';
        
        // <tr id="'+name_list+'_row_list" class="row_list" style="display: none;"> \
        //    </tr> 
        
        $(this).html(table_base);
        
        //Connect with ajax. 
        if(!options.hasOwnProperty("url"))  {
            

            list_tr='<tr class="title_list"> \
            <td>No ajax url defined</td> \
            </tr>';

            
            
            $(this).children('#'+name_list).append(list_tr);
            
            //alert(new_title_list.attr('id'));
/*
            $(new_title_list).html('');*/
            
            //new_title_list.show();
            return false;
            
        }
        
        exclude_list=[];
        
        if(options.hasOwnProperty("exclude_list"))  {
            
            exclude_list=options.exclude_list;
            
        }

        this.order_field=0;
        
        if(options.hasOwnProperty("order_field"))  {
            
            this.order_field=options.order_field;
            
        }
        
        this.order=0;
        
        if(options.hasOwnProperty("order"))  {
            
            this.order=options.order;
            
        }
        
        arr_icon=['<i class="fa fa-arrow-down asc_icon" aria-hidden="true"></i>', '<i class="fa fa-arrow-up desc_icon" aria-hidden="true"></i>'];
        
        var func=this;
        
        this.updateAjax=function (name_list, options, position) {
        
            // Get ajax, the ajax request need give a json request with name titles and fields value. 
            
            $('#'+name_list).html('');    
            
            $('.pages').remove();
            
            $('.link_pages').off();
            $('.change_order').off();
            
            extra_data={}
            
            if(options.hasOwnProperty("extra_data"))  {
                
                extra_data=options.extra_data;
                
            }
            
            data={position: position, order_field: func.order_field, order: func.order};
            
            for(i in extra_data) {
                
                data[i]=extra_data[i];
                
            }
            
            var block_name=$('#'+name_list).parent();
            
            $.ajax({
                type: 'POST',
                url: options.url,
                data: data,
                success: function (data) {
                    
                    order_field='';
                    
                    list_tr='<tr class="title_list">';
                    
                    for(i in data.fields)
                    {
                        
                        if(data.fields[i][1])
                        {
                        
                            list_tr+='<td><a href="#'+i+'" class="change_order" id="field_list_'+i+'">'+data.fields[i][0]+'</a></td>';
                            
                        }
                        else
                        {

                            list_tr+='<td>'+data.fields[i][0]+'</td>';
                            
                        }
                        
                    }

                    list_tr+='</tr>';
                    
                    $('#'+name_list).append(list_tr);
                    
                    list_tr='';
                    
                    for(i in data.rows)
                    {

                        list_tr+='<tr class="row_list">';

                        for(n in data.rows[i]) {

                            
                            if(exclude_list.indexOf(n)==-1)
                            {
                                
                                list_tr+='<td class="'+n+'_td">'+data.rows[i][n]+'</td>';
                                
                            }
                        }
                        
                        list_tr+='</tr>';                    
                        
                    }
                    $('#'+name_list).append(list_tr);

                    block_name.prepend('<p class="pages">'+data.html_pages+'</p>');                    
                    block_name.append('<p class="pages">'+data.html_pages+'</p>');
                    
                    // Prepare defaults of fields orders
                    

                    $('#field_list_'+func.order_field).append(' '+arr_icon[func.order]);
                    
                    // Prepare button events
                    
                    $('.link_pages').click(function (e) {
                        
                        e.preventDefault();
                    
                        position=parseInt($(this).attr('href').replace('#?begin_page=', ''));                    
                        
                        if(isNaN(position))
                        {
                            
                            position=0;
                            
                        }
                        
                        
                        func.updateAjax(name_list, options, position);       
                        
                        
                        
                    });
                    
                    $('.change_order').click(function (e) {
                    
                        e.preventDefault();
                        
                        func.order_field=$(this).attr('href').replace('#', '');
                        
                        //alert($(this).children('i').attr('class'));

                        if($(this).children('i').hasClass('asc_icon'))
                        {

                            func.order=1;

                        }
                        else
                        {

                            func.order=0;
                            
                        }
                        
                        func.updateAjax(name_list, options, position);       
                    
                    });
                    
                    
                },
                error: function (data) {
                    console.log('Cannot connect to list source');
                },
                dataType: 'json'
            });
        
        };
        
        this.updateAjax(name_list, options, 0, '');
        
        return this;
    }

})( jQuery );
